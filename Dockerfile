#### Stage 1 : Pull official base image
FROM python:3.10

#### Stage 2 : Set the current docker author email address inside the image
LABEL maintainer="aymarmbobda1597@gmail.com"

#### Stage 3 : Change timezone base on project
ENV TZ=Europe/Paris 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#### Stage 4 : Set environment variables
ENV DEBUG 0
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

#### Stage 5 : Create a working directory for image
RUN mkdir -p /webapp

#### Stage 6 : Set the current working directory inside the image
WORKDIR /webapp

#### Stage 7 : Copy dependencies file to the image
COPY ./requirements.txt /webapp/

#### Stage 8 : Install image dependencies
RUN python3 -m pip install --upgrade pip
RUN pip install -r requirements.txt

#### Stage 9 : Copy files in the image
COPY . /webapp/

#### Stage 10 : Create directory for upload files
RUN mkdir /webapp/uploads

# #### Stage 11 : Expose docker image port
EXPOSE 80

#### Stage 12 : Copy dependencies file to the image
RUN chmod +x /webapp/docker-entrypoint.sh

#### Stage 13 : Launch the application
CMD [ "sh", "/webapp/docker-entrypoint.sh" ]