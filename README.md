#   World Datas
##  Installation de l'environement de developpement

-   **Visual Studio Code** : éditeur de code extensible développé par Microsoft pour **Windows**, **Linux** et **macOS**. Les fonctionnalités incluent la prise en charge du débogage, la mise en évidence de la syntaxe, la complétion intelligente du code, les snippets, la refactorisation du code et Git intégré.

-   **PgAdmin** : principal outil de gestion Open Source pour Postgres, la base de données Open Source la plus avancée au monde. pgAdmin 4 est conçu pour répondre aux besoins des utilisateurs Postgres novices et expérimentés, en fournissant une interface graphique puissante qui simplifie la création, la maintenance et l'utilisation des objets de base de données.

-   **Adminer** : application Web offrant une interface graphique pour plusieurs systèmes de gestion de base de données, réalisée en PHP et distribuée sous licence Apache. Il se présente comme une alternative légère à phpMyAdmin et a pour particularité d'être entièrement contenu dans un seul fichier PHP.

-   **Swagger** : langage de description d'interface pour décrire les API RESTful exprimées à l'aide de JSON. Swagger est utilisé avec un ensemble d'outils logiciels open source pour concevoir, créer, documenter et utiliser les services Web RESTful.

##  Lancement de l'application

Pour lancer le projet, il faut tout d’abord le cloner projet et fonction du choix de développement il y a 2 possibilités pour le lancement soit directement avec Python dans la machine ou en utilisant Docker. Si vous optez pour Python directement alors il vous faudra installer les outils suivants: **Redis**, **PostgreSql**, **Minio** et bien évidemment **Python** pour pouvoir lancer le projet.

-   $>  **python3 -m venv venv** : Utilisez cette commande pour créer un environement virtuel python.
**NB: Vous ne le faites que si vous n’utilisez pas docker**.

-   $>  **source ./venv/bin/activate** : Utilisez cette commande pour activer l'environement virtuel python nouvellement crée.
**NB: Vous ne le faites que si vous n’utilisez pas docker, modifier par le chemin d'access correct si vous est sur windows**.

-   $>  **pip install -r requirements.txt** : Utilisez cette commande pour installer les dépendances du projet.
**NB: Vous ne le faites que si vous n’utilisez pas docker**.

-   $>  **python3 manage.py migrate** : utiliser pour effectuer les migration dans la base de données.
**NB: Vous ne le faites que si vous n’utilisez pas docker**.

-   $>  **sh start.sh** : utiliser pour lancer l’API en local.
**NB: Vous ne le faites que si vous n’utilisez pas docker**.

-   $>  **docker-compose up** : Utilisez pour lancer l’API lorsque la configuration de docker est mise en place dans le fichier docker-compsoe.yml et Dockerfile.

-   $>  **docker-compose run world_datas_api python manage.py migrate** : Utilisez pour lancer les migrations.

##  Acceder a la documentation

Allez maintenant sur http://127.0.0.1:808/docs

Vous verrez la documentation API interactive automatique (fournie par Swagger UI).

##