#!/bin/sh

#   Section 1- Bash options

set -o errexit  
set -o pipefail  
set -o nounset

#   Section 2: Health of dependent services

postgres_ready() {  
    python << END  
import sys

from psycopg2 import connect  
from psycopg2.errors import OperationalError

try:  
    connect(  
        dbname="${POSTGRES_DATABASE}",  
        user="${POSTGRES_USERNAME}",  
        password="${POSTGRES_PASSWORD}",  
        host="${POSTGRES_HOST}",  
        port="${POSTGRES_PORT}",  
    )  
except OperationalError:  
    sys.exit(-1)  
END  
}

#   Section 3- Idempotent Django commands

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
# python manage.py makemigrations
python manage.py migrate

# Start server
echo "Starting server"
# python manage.py runserver 0.0.0.0:80
gunicorn world_datas.wsgi:application --bind 0.0.0.0:80

exec "$@"