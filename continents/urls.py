#   encoding: utf-8

#   Import Modules
from django.urls import path

from .views import ContinentList, ContinentDetail

#   Variables Definitions
urlpatterns = [
    path('', ContinentList.as_view(), name='Continents_list'),
    path('<str:id>', ContinentDetail.as_view(), name='Continents_details'),
]