from email.policy import default
import uuid
from django.db import models

# Create your models here.
class Continent(models.Model):
    
    uuid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    
    name = models.JSONField(null = True, default = dict)
    
    created_at = models.DateTimeField(auto_now_add=True, null = False, blank = False)
    modified_at = models.DateTimeField(auto_now=True, null = False, blank = False)
    
    class Meta:
        # db_table = 'Continents'
        ordering = ['-created_at']
    
    def __str__(self):
        return f'<Continent: uuid: {self.uuid} />'