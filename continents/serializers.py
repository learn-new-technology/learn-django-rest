from rest_framework import serializers

from .models import Continent

#   Serializers Definitions
class ContinentSerializerInput(serializers.Serializer):
    uuid = serializers.UUIDField(required = False)
    name = serializers.JSONField(required = False)
    
    def create(self, validated_data):
        """
            Create and return a new `Continent` instance, given the validated data.
        """        
        return Continent.objects.create(**validated_data)


    def update(self, instance, validated_data):
        """
            Update and return an existing `Continent` instance, given the validated data.
        """        
        instance.name = validated_data.get('name', instance.name)
        
        instance.save()
        return instance

class ContinentSerializerOutput(serializers.Serializer):
    uuid = serializers.UUIDField(required = False)
    name = serializers.JSONField(required = False)    
    
    created_at = serializers.DateTimeField(read_only = True, required = False)
    modified_at = serializers.DateTimeField(read_only = True, required = False)

class ContinentSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    name = serializers.JSONField(required = False)