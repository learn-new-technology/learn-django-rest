#   encoding: utf-8

#   Import Modules
import math

from drf_yasg import openapi
from rest_framework import status
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from django.views.decorators.csrf import csrf_exempt
from utils.schemas import DataList, Msg, DataListSerializer, MsgSerializer

from .models import Continent
from .serializers import ContinentSerializerInput, ContinentSerializerOutput

# Create your views here.
class ContinentList(APIView):
    page = openapi.Parameter('page', openapi.IN_QUERY, type = openapi.TYPE_INTEGER, default = 1)
    keyword = openapi.Parameter('keyword', openapi.IN_QUERY, type = openapi.TYPE_STRING, default = '')
    per_page = openapi.Parameter('per_page', openapi.IN_QUERY, type = openapi.TYPE_INTEGER, default = 25)
    order = openapi.Parameter( 'order', in_ = openapi.IN_QUERY, type = openapi.TYPE_STRING, enum = ['asc', 'desc'], default = 'desc')


    @swagger_auto_schema(
        operation_description = "This function creates and returns information from a continent",
        request_body = ContinentSerializerInput,
        responses = { 
            201 : ContinentSerializerOutput
        },
    )
    @csrf_exempt
    def post(self, request):
        serializer = ContinentSerializerInput(data = request.data)
        
        if serializer.is_valid(raise_exception = True):
            continent = serializer.save()
        
        continent_ouput = ContinentSerializerOutput(continent)
        return JsonResponse(status = status.HTTP_201_CREATED, data = continent_ouput.data, safe = False)
    
    @swagger_auto_schema(
        operation_description = "This function searches and returns a list of continents",
        manual_parameters = [page, order, keyword, per_page],
        responses = { 
            200 : DataListSerializer
        },
    )
    @csrf_exempt
    def get(self, request, *args, **kwargs):
        page = int(request.query_params.get('page')) if request.query_params.get('page') else 1
        order = str(request.query_params.get('order')) if request.query_params.get('order') else 'desc'
        keyword = str(request.query_params.get('keyword')) if request.query_params.get('keyword') else None
        per_page = int(request.query_params.get('per_page')) if request.query_params.get('per_page') else 100
        
                
        record_query = Continent.objects
        
        if keyword :
            record_query = record_query.filter(locale__contains = keyword)
        
        if order in ["asc", "ASC"]:
            record_query = record_query.order_by('created_at')
        
        if order in ["desc", "DESC"]:
            record_query = record_query.order_by('-created_at')
        
        total = record_query.count()
        result = record_query.all()[((page - 1) * per_page) : per_page]
        serializer = ContinentSerializerOutput(result, many = True)
        dataListSerializer = DataListSerializer(DataList(total = total, pages = math.ceil(total / per_page), current_page = page, per_page = per_page, data = serializer.data))
        
        return JsonResponse(status = status.HTTP_200_OK, data = dataListSerializer.data, safe = False)


class ContinentDetail(APIView):
    
    def get_object(self, id):
        try:
            return Continent.objects.get(pk = id)
        except Continent.DoesNotExist:
            return None
    
    @swagger_auto_schema(
        operation_description = "This function searches and returns the details of a continent using its id",
        responses = { 
            200 : ContinentSerializerOutput,
            404 : MsgSerializer
        },
    )
    @csrf_exempt
    def get(self, request, id, *args, **kwargs):
        continent = self.get_object(id = id)
        
        if not continent:
            msg_serializer = MsgSerializer(Msg('Continent not found'))
            return JsonResponse(status = status.HTTP_404_NOT_FOUND, data = msg_serializer.data, safe = False)
        
        serializer = ContinentSerializerOutput(continent)
        
        return JsonResponse(status = status.HTTP_200_OK, data = serializer.data, safe = False)

    @swagger_auto_schema(
        operation_description = "This function searches, updates and return the information of an existing continent with its id and the new datas of the latter",
        request_body = ContinentSerializerInput,
        responses = { 
            200 : ContinentSerializerOutput,
            400 : 'Bad Request',
            404 : MsgSerializer
        },
    )
    @csrf_exempt
    def put(self, request, id, *args, **kwargs):
        continent = self.get_object(id = id)
        
        if not continent:
            msg_serializer = MsgSerializer(Msg('Continent not found'))
            return JsonResponse(status = status.HTTP_404_NOT_FOUND, data = msg_serializer.data, safe = False)
        
        serializer = ContinentSerializerInput(instance = continent, data = request.data, partial = True)
        
        if serializer.is_valid(raise_exception = True):
            continent = serializer.save()
            continent_ouput = ContinentSerializerOutput(continent)
            return JsonResponse(status = status.HTTP_200_OK, data = continent_ouput.data, safe = False)
        
        return JsonResponse(status = status.HTTP_400_BAD_REQUEST, data = serializer.errors, safe = False)
    
    @swagger_auto_schema(
        operation_description = "This function searches and deletes data from a continent",
        responses = { 
            200 : MsgSerializer,
            204 : MsgSerializer,
            404 : MsgSerializer 
        },
    )
    @csrf_exempt
    def delete(self, request, id, *args, **kwargs):
        continent = self.get_object(id = id)
        
        if not continent:
            msg_serializer = MsgSerializer(Msg('Continent not found'))
            return JsonResponse(status = status.HTTP_404_NOT_FOUND, data = msg_serializer.data, safe = False)
        
        continent.delete()
        msg_serializer = MsgSerializer(Msg('Continent not found'))
        return JsonResponse(status = status.HTTP_204_NO_CONTENT, data = msg_serializer.data, safe = False)