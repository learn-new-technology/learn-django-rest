"""world_datas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from drf_yasg import openapi
from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view as swagger_get_schema_view

#   Variables Definitions
license = { 'name' : 'Apache 2.0', 'url' : 'http://springdoc.org'}
contact = { 'name' : 'MAI', 'email' : 'mai@email.com', 'url' : 'http://mai.com'}
schema_view = swagger_get_schema_view(
    openapi.Info(
        title = 'World Datas API',
        default_version = 'V1',
        description = "API documentation of application that wil return informations about a country",
        terms_of_service = 'https://www.google.com/policies/terms/',
        contact = openapi.Contact(name = 'MAI', email='mai@email.com', url = 'http://mai.com'),
        license = openapi.License(name = 'Apache 2.0', url = 'http://springdoc.org'),
    ),
    public = True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout = 0), name = 'schema-redoc'),
    path('docs/', schema_view.with_ui('swagger', cache_timeout = 0), name = 'swagger-schema'),
    path('api/v1/', 
        include([
            path('countries/', include(('countries.urls', 'Country'), namespace = 'Country')),
            path('languages/', include(('languages.urls', 'language'), namespace = 'languages')),
            path('continents/', include(('continents.urls', 'Continents'), namespace = 'Continent')),
        ])
    ),
]
