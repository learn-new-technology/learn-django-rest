#   encoding: utf-8

#   Import Modules
from rest_framework import serializers

#   Class Definitions
class DataList():

    def __init__(self, total, pages, current_page, per_page, data) -> None:
        self.total = total
        self.pages = pages
        self.current_page = current_page
        self.per_page = per_page
        self.data = data

class Msg():
    
    def __init__(self, message) -> None:
        self.message = message

#   Serializes of class definited
class DataListSerializer(serializers.Serializer):
    total = serializers.IntegerField(read_only = True)
    pages = serializers.IntegerField(read_only = True)
    current_page = serializers.IntegerField(read_only = True)
    per_page = serializers.IntegerField(read_only = True)
    data = serializers.ListField(read_only = True)

class MsgSerializer(serializers.Serializer):
    message = serializers.CharField(required = True, max_length = 250)
