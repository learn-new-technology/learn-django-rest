import uuid
from django.db import models

# Create your models here.
class Language(models.Model):
    
    uuid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    
    locale = models.CharField(max_length = 50, null = True, default = '')
    
    created_at = models.DateTimeField(auto_now_add=True, null = False, blank = False)
    modified_at = models.DateTimeField(auto_now=True, null = False, blank = False)
    
    class Meta:
        # db_table = 'Langues'
        ordering = ['-created_at']
    
    def __str__(self):
        return f'<Language: uuid: {self.uuid} />'