from rest_framework import serializers

from .models import Language

#   Serializers Definitions
class LanguageSerializerInput(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    locale = serializers.CharField(required = False, allow_blank = True, max_length = 25)
    
    def create(self, validated_data):
        """
            Create and return a new `Snippet` instance, given the validated data.
        """        
        return Language.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
            Update and return an existing `Snippet` instance, given the validated data.
        """        
        instance.locale = validated_data.get('locale', instance.locale)
        
        instance.save()
        return instance

class LanguageSerializerOutput(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    locale = serializers.CharField(required = False, allow_blank = True, max_length = 25)
    
    created_at = serializers.DateTimeField(read_only = True, required = False)
    modified_at = serializers.DateTimeField(read_only = True, required = False)

class LanguageSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    locale = serializers.CharField(required = False, allow_blank = True, max_length = 25)
    