#   encoding: utf-8

#   Import Modules
from django.urls import path

from .views import LanguageList, LanguageDetail

#   Variables Definitions
urlpatterns = [
    path('', LanguageList.as_view(), name='Languages_list'),
    path('<str:id>', LanguageDetail.as_view(), name='languages_details'),
]