#   encoding: utf-8

#   Import Modules
from django.urls import path

from .views import CountryList, CountryDetail

#   Variables Definitions
urlpatterns = [
    path('', CountryList.as_view(), name='Countries_list'),
    path('<str:id>', CountryDetail.as_view(), name='Countries_details'),
]