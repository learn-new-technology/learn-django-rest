from rest_framework import serializers

from .models import Country
from continents.models import Continent
from continents.serializers import ContinentSerializer

#   Serializers Definitions
class CountryCodeSerializer(serializers.Serializer):
    two_letter = serializers.CharField(required = False, max_length = 25)
    three_letter = serializers.CharField(required = False, max_length = 25)
    numeric = serializers.IntegerField(required = False)

class CountrySerializerInput(serializers.Serializer):
    uuid = serializers.UUIDField(required = False)
    name = serializers.JSONField(required = False)
    abbreviation_code = CountryCodeSerializer(required = False)
    phone_code = serializers.CharField(required = False, allow_blank = True, max_length = 6)
    surface_area = serializers.DecimalField(required = False, max_digits = 10, decimal_places = 2)

    continent = serializers.PrimaryKeyRelatedField(required = True, queryset = Continent.objects.all(), many = False)
    
    def create(self, validated_data):
        """
            Create and return a new `Country` instance, given the validated data.
        """        
        return Country.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
            Update and return an existing `Country` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.phone_code = validated_data.get('phone_code', instance.phone_code)
        instance.surface_area = validated_data.get('surface_area', instance.surface_area)
        instance.abbreviation_code = validated_data.get('abbreviation_code', instance.abbreviation_code)
        
        instance.save()
        return instance

class CountrySerializerOutput(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    name = serializers.JSONField(required = False)
    abbreviation_code = CountryCodeSerializer(required=False)
    phone_code = serializers.CharField(required = False, allow_blank = True, max_length = 6)
    surface_area = serializers.DecimalField(required = False, max_digits = 10, decimal_places = 2)
    
    continent = ContinentSerializer(required = False, read_only = True, many = False)
    
    created_at = serializers.DateTimeField(read_only = True, required = False)
    modified_at = serializers.DateTimeField(read_only = True, required = False)

class CountrySerializer(serializers.Serializer):
    uuid = serializers.UUIDField(read_only = True)
    name = serializers.JSONField(required = False)
    abbreviation_code = CountryCodeSerializer(required=False)
    phone_code = serializers.CharField(required = False, allow_blank = True, max_length = 6)
    surface_area = serializers.DecimalField(required = False, max_digits = 10, decimal_places = 2)