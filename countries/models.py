import uuid
from django.db import models
from languages.models import Language
from continents.models import Continent

# Create your models here.
class Country(models.Model):
    
    uuid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    
    name = models.JSONField(null = True, default = dict)
    abbreviation_code = models.JSONField(null = True, default = dict)
    phone_code = models.CharField(max_length = 5, null = False, default = '')
    surface_area = models.DecimalField(null = False, max_digits = 10, decimal_places = 2, default = 0.99)
    
    #   One To Many
    continent = models.ForeignKey(Continent, related_name = 'continent', null = True, blank = True, on_delete = models.CASCADE)
    
    #   Many To Many
    # languages = models.ManyToManyField(Language)
    
    created_at = models.DateTimeField(auto_now_add=True, null = False, blank = False)
    modified_at = models.DateTimeField(auto_now=True, null = False, blank = False)
    
    class Meta:
        # db_table = 'Countries'
        ordering = ['-created_at']
        
    def __str__(self):
        return f'<Country: uuid: {self.uuid} />'    